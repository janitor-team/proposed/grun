grun (0.9.3+git20200303-1) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * debian/control: Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Wed, 18 Aug 2021 11:09:37 -0400

grun (0.9.3+git20200303-1~exp1) experimental; urgency=medium

  * QA upload.
  * New upstream snapshot from git trunk.
    + Properly handle --preload option. Closes: #300296
    + Apply Swedish PO translation. Closes: #348254
  * debian/control:
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.5.1.
  * debian/install: Install both original xpm file and 32x32 file.
  * debian/grun.desktop: Use desktop file to replace menu file.

 -- Boyuan Yang <byang@debian.org>  Sat, 10 Apr 2021 17:05:14 -0400

grun (0.9.3-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer.
  * Using dh-autoreconf instead of autotools_dev.
  * debian/control
      - Bumped Standards-Version to 3.9.6.
      - Update DH level to 9.
      - Include ${misc:Depends} at Depends.
      - Updated Homepage field (Closes: #783222).
      - Fixed a typo in long description, remove extra space (Closes: #750862).
      - Fixed GTK+ spell.
  * debian/copyright
      - Using 1.0 format.
      - Full updated.
  * debian/rules
      - To new (reduced) format.
      - Include DEB_BUILD_MAINT_OPTIONS for hardening.
  * Create debian/install to install icon xpm.
  * Create debian/watch.
  * Patch to fix spell of word persistent in grun.c.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Mon, 27 Jul 2015 13:03:00 -0300

grun (0.9.3-1) unstable; urgency=low

  * New maintainer. Many thanks to oohara for all his previous work.
  * New upstream Release (Closes: #461171).
   - Most Debian patches have been integrated upstream, so our diff is 
    now contained within /debian.
   - gassoc and consfile are now shipped in sysconfdir upstream, so
    there's no longer a need to move them. Removed postinst and prerm 
    snippets to symlink /usr/share/grun.
   - Uses dynamic layout for widgets (Closes: #445466).
  * Remove autoconf and automake1.7 from Build-depends, we don't need to
    regenerate the build system anymore.
  * Remove pkg-config from Build-depends, it's required by libgtk2.0-dev
  anyways.
  * Remove Conflicts: gnome-utils (<= 1.0.50-5). Even oldstable has 2.8.1-1.
  * Remove tricks from debian/rules to regenerate *gmo files, and clean up
   useless commented out commands.
  * Move the menu entry to Applications/File Management, to comply with the new
   menu policy.
  * Update to debhelper compat mode 5, and upgrade the build-dependency
   accordingly.
  * Tweak debian/rules to use --host, --build, CFLAGS and LDFLAGS.
  * Update to Standards-Version 3.7.3. No further changes needed.
  
 -- Luis Rodrigo Gallardo Cruz <rodrigo@debian.org>  Sun, 27 Jan 2008 00:19:36 -0600

grun (0.9.2-14.1) unstable; urgency=low

  * NMU.
  * grun.c: Call bind_textdomain_codeset() to have gettext recode strings
    to UTF-8. (Closes: #438704).
  * po/*.po: Set ISO-8859-1 encoding headers.
  * debian/rules: Remove po/*gmo files on configure, to force regenerating
    with correct encoding headers.
  
 -- Luis Rodrigo Gallardo Cruz <rodrigo@nul-unu.com>  Mon, 24 Sep 2007 14:26:41 -0500

grun (0.9.2-14) unstable; urgency=low

  * grun.c: yet another fix of auto-completion (closes: #281334)
    + removed the region selection in auto-completion; gtk assumes that
      the cursor is always at the last of the selection if there is
      any selection
    + if the entire command is selected, if the cursor is at the last
      of the selection and if the user types a new character, then
      replace the entire command with that character, don't append it
    + when searching for a directory, ignore "." and ".."
    + the program respects the slash at the beginning of the command
      when the user deletes a character as well as when he/she adds one
  * debian/README.Debian: noted the assumptions of auto-completion
  * grun.c: move the cursor first, then select the region

 -- Oohara Yuuma <oohara@debian.org>  Tue,  7 Dec 2004 20:35:04 +0900

grun (0.9.2-13) unstable; urgency=low

  * grun.c: hope to fix the auto-completion
    + now the completion is based on the position of the cursor,
      not on the number of key-typing
    + If the first character of the command is not a slash, the program
      searches the history, then searches the PATH.  This may be a problem
      if a command in the history has command line options, but I can't
      help it because the program should not assume that the command itself
      has no space character in its name.  Note that an explicit completion
      by the TAB key is an exception and it does not search the history.
    + if the user added a new character to the command and if it is the first
      character that does not match any executable, then discard the result
      of the previous auto-completion
  * debian/menu: quoted strings
  * grun.1x: escaped hyphens
  * debian/control: added xterm to Suggests: as an example of
    x-terminal-emulator

 -- Oohara Yuuma <oohara@debian.org>  Mon,  6 Dec 2004 00:13:34 +0900

grun (0.9.2-12) unstable; urgency=low

  * grun.c: disabled the window icon to avoid the segfault problem
    (closes: #252186)
  * grun.c: added the declaration of functions

 -- Oohara Yuuma <oohara@debian.org>  Thu,  3 Jun 2004 09:51:46 +0900

grun (0.9.2-11) unstable; urgency=low

  * grun.c: changed the behavior of ESC; now it is "cancel" instead of
    auto-complete (thanks to Tollef Fog Heen <tfheen@raw.no> for the
    reverse patch) (closes: #200131)
  * debian/README.Debian: new file, documented the ESC change
  * debian/rules: dropped DEB_BUILD_OPTIONS debug and added noopt
  * debian/control: set Standards-Version: to 3.5.10
  * debian/control: added upstream webpage URL, which is completely useless
    because it is dead but anyway

 -- Oohara Yuuma <oohara@debian.org>  Mon,  7 Jul 2003 23:09:04 +0900

grun (0.9.2-10) unstable; urgency=low

  * rebuild for gtk 2.0 (thanks to Tollef Fog Heen <tfheen@raw.no>
    and Josip Rodin <joy@srce.hr> for help) (closes: #190097)
  * debian/control: Build-Depends: libgtk2.0-dev and automake1.7
  * debian/control: Build-Conflicts: libgtk1.2-dev because dpkg happily
    installs both libgtk1.2-dev and libgtk2.0-dev at the same time
  * debian/conffiles: removed because all the conffiles are in /etc,
    which is handled by debhelper
  * debian/control: Build-Depends: debhelper (>> 4.0.0) for automatic
    conffile marking
  * configure.in: uses AM_PATH_GTK_2_0 instead of AM_PATH_GTK
  * debian/rules: updates config.{guess,sub} on clean
  * debian/control: Build-Depends: pkg-config because ./configure calls it
  * aclocal.m4, configure: regenerated
  * configure.in: no longer overwrites po/Makefile with ./configure
    default commands
  * debian/copyright: there is only one upstream author
  * debian/rules: removes config.log on clean
  * debian/grun.xpm: regenerated with the following imagemagick command:
    mogrify -format xpm -geometry 32x32 -map \
      /usr/X11R6/include/X11/pixmaps/cmap.xpm grun2.xpm

 -- Oohara Yuuma <oohara@debian.org>  Mon, 23 Jun 2003 21:38:22 +0900

grun (0.9.2-9) unstable; urgency=low

  * new maintainer
  * adopting an orphaned package (closes: #131205)
  * grun.c: added cast to strsep(), declared the return value of main()
    as int and added dummy return to main() to avoid gcc warnings
  * grun.c: added #include <locale.h> so that it can build without gcc
    -O2 flag (LC_ALL must be defined)
  * Makefile.am: added CFLAGS to build without gcc -g flag by default
  * Makefile.am, debian/rules: added DEB_BUILD_OPTIONS "debug" and
    "nostrip" support
  * grun.c: added #include <string.h> and removed cnt from main() to
    avoid gcc -Wall warnings
  * debian/control: added gettext to Build-Depends:
  * consfile: added many commands
  * gassoc: removed comments because they are not allowed
  * grun.c: fixed search path for the global configuration files
  * gassoc: s/gnome-gv/gv/, s/gedit/sensible-editor/

 -- Oohara Yuuma <oohara@debian.org>  Sat, 23 Feb 2002 02:57:47 +0900

grun (0.9.2-8) unstable; urgency=low

  * Setting maintainer to Debian QA, orphaning the package

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 27 Jan 2002 23:38:55 -0200

grun (0.9.2-7) unstable; urgency=low

  * debian/control: Minor corrections to the packages description
    thanks to Matt Zimmerman.
  
 -- Gustavo Noronha Silva <kov@debian.org>  Fri, 23 Nov 2001 12:15:24 -0200

grun (0.9.2-6) unstable; urgency=low

  * copyright: debianized by me =)
  * rules: changed to DH_COMPAT 3

 -- Gustavo Noronha Silva <kov@debian.org>  Sun,  1 Jul 2001 11:53:04 -0300

grun (0.9.2-5) unstable; urgency=low

  * Rebuild with dpkg-dev version 1.9.14.1 (hope this helps as I couldn't
  find 1.9.10 on the archive)

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 30 Jun 2001 03:45:33 -0300

grun (0.9.2-4) unstable; urgency=low

  * menu: Added icon to menu
  * debian/grun.xpm: the icon =) it is installed in /usr/share/pixmaps

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 25 Jun 2001 23:02:32 -0300

grun (0.9.2-3) unstable; urgency=low

  * New Maintainer

 -- Gustavo Noronha Silva <kov@debian.org>  Sat,  9 Jun 2001 14:42:38 -0300

grun (0.9.2-2) unstable; urgency=low

  * Added a conflicts entry in control to fix a package overlap with the
    gnome-utils package in Debian testing; closes: #90319 
  * Moved section from misc to x11 to fix override disparity.
  * Corrected a typo in copyright file.
  * Removed call of deprecated dh_testversion in rules.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Mon, 26 Mar 2001 17:50:26 +0200

grun (0.9.2-1) unstable; urgency=low

  * Initial Release; closes: #88330.
  * Modified the files gassoc and consfile.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat,  3 Mar 2001 11:19:48 +0100
